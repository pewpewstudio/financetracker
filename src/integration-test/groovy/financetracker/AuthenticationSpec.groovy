package financetracker

import geb.driver.CachingDriverFactory
import grails.test.mixin.integration.Integration
import grails.transaction.*
import geb.spock.*
import org.springframework.beans.factory.annotation.Autowired

/**
 * See http://www.gebish.org/manual/current/ for more instructions
 */
@Integration
@Rollback
class AuthenticationSpec extends GebSpec {

    @Autowired
    AuthService authService
    User user

    def setup() {
        user = authService.createUser("TEST_USER_1", "1234567")
    }

    def cleanup() {
        CachingDriverFactory.clearCache()
    }

    void "login for existing user"() {
        when: "The home page is visited"
            go '/'

        then: "Click login button"
            $("a", text: "Вход").click()

        then: "Ensure we are at the right place"
            $(".form-top-left > h3").text() == "Вход"

        then: "Fill login form"
            $("input", name: "login").value("TEST_USER_1")
            $("input", name: "password").value("1234567")

        then: "Click login button"
            $("button", type: "submit").click()

        then: "Ensure we are at the dashboard"
            $("#user-profile-menu").text() == user.login
    }
}
