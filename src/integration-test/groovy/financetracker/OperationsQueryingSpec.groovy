package financetracker


import grails.test.mixin.integration.Integration
import grails.transaction.*
import org.pewpew.util.Hasher
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.*

@Integration
@Rollback
class OperationsQueryingSpec extends Specification {

    @Autowired
    OperationService operationService

    def setup() {
    }

    def cleanup() {
    }

    void "test operations chart data"() {
        given:"Create test data"
            def now = new Date()
            def user = new User(login: "at_user", passwordHash: Hasher.sha256("123"), registered: now).save(flush: true)
            def account = new Account(user: user, name: "at_account", cardNumber: "4024007173904615", baseSum: 200.0).save(flush: true)
            new Operation(account: account, created: now + 1, value: 305.0, description: "o1").save(flush: true)
            new Operation(account: account, created: now + 2, value: 450.0, description: "o2").save(flush: true)

        when:"Require data"
            def result = operationService.getOperationsForChart(account)

        then:
            result[0].cumulativeValue == 505.0
            result[1].cumulativeValue == 955.0
    }
}
