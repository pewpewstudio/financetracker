import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver

waiting {
    timeout = 2
}

driver = {
    System.setProperty("webdriver.firefox.bin", "C:/Program Files (x86)/Mozilla Firefox/firefox.exe")
    System.setProperty("webdriver.chrome.driver", "c:/bin/chromedriver.exe")
    new ChromeDriver()
}