package org.pewpew.util

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class HasherSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "sha256 algorithm"() {
        def data = "test"
        def expected = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"

        expect:
            Hasher.sha256(data) == expected
    }

    void "binary data to string converter"() {
        byte[] data = [1, 5, 20]
        def expected = "010514"

        expect:
            Hasher.hashToString(data) == expected
    }
}