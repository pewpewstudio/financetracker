package org.pewpew.util

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class StringFormatSpec extends Specification {

    void "string truncate"() {
        expect:
            StringFormat.truncateFormatted(str, padding, placeholder) == expected

        where:
            str          | padding | placeholder | expected
            "ABCDE12345" | 2       | ".."        | "AB..45"
            "ABCDE12345" | 5       | ".."        | "ABCDE12345"
            "ABCDE12345" | 2       | "........." | "ABCDE12345"
    }
}