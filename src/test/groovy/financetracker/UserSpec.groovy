package financetracker

import grails.test.mixin.TestFor
import org.pewpew.util.Hasher
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.Month
import java.time.format.DateTimeFormatter

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(User)
class UserSpec extends Specification {

    void "to string"() {
        given:
            def date = new Date(2017 - 1900, 1, 10, 14, 10, 25)

            User user = new User()
            user.login = "username"
            user.passwordHash = Hasher.sha256("123456")
            user.registered = date

        expect:
            user.toString() == expected

        where:
            isActive | expected
            true     | "username, registered at: 10.02.2017 14:10:25"
    }
}
