package financetracker

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AuthService)
@Mock(User)
class AuthServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "password encryption"() {
        given:
            def password = "12345678"
            def salt = "2011-12-03T10:15:30"
            def expectedHash = "1ce8013456355ff25b7fbd47f295ad0a90e3d26ee82bae77b6cb0b3103e56c4d"

        expect:
            expectedHash == service.hashPassword(password, salt)
    }
}
