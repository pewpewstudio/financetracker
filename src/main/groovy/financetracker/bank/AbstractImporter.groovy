package financetracker.bank

import org.springframework.core.io.InputStreamSource

interface AbstractImporter {
    int read(InputStreamSource source)
}