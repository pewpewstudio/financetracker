package financetracker.bank.tinkoff

import financetracker.bank.DataImportException

class TinkoffImportException extends DataImportException {
    TinkoffImportException(String message, Throwable cause) {
        super(message, cause)
    }

    TinkoffImportException(String message) {
        super(message)
    }
}
