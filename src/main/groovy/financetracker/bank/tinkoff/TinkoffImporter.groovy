package financetracker.bank.tinkoff

import financetracker.bank.AbstractImporter
import org.apache.commons.csv.CSVFormat
import org.springframework.core.io.InputStreamSource
import java.text.SimpleDateFormat

class TinkoffImporter implements AbstractImporter {

    private static final int FILE_FIELDS_NUMBER = 13

    private static final int OPERATION_DATE = 0
    private static final int PAYMENT_DATE = 1
    private static final int CARD_NUMBER_TAIL = 2
    private static final int STATE = 3
    private static final int OPERATION_VALUE = 4
    private static final int OPERATION_CURRENCY = 5
    private static final int PAYMENT_VALUE = 6
    private static final int PAYMENT_CURRENCY = 7
    private static final int CASH_BACK_VALUE = 8
    private static final int CATEGORY = 9
    private static final int MERCHANT_CATEGORY_CODE = 10
    private static final int DESCRIPTION = 11
    private static final int BONUS_VALUE = 12

    List<TinkoffOperation> operations

    TinkoffImporter() {
        operations = []
    }

    int read(InputStreamSource source) {
        def reader = source.inputStream.newReader()
        reader.readLine()

        def data = CSVFormat.DEFAULT.withSkipHeaderRecord(true).withDelimiter(';' as Character).parse(reader)

        def dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss")

        int linesRead = 0

        try {
            data.records.eachWithIndex { fields, i ->
                if (fields.size() != FILE_FIELDS_NUMBER) {
                    throw new TinkoffImportException("Number of fields in data file is ${fields.size()}, await: ${FILE_FIELDS_NUMBER}")
                }

                def operation = new TinkoffOperation()

                def operationDate = fields[OPERATION_DATE]
                operation.operationDate = operationDate ? dateFormat.parse(operationDate) : null
                def paymentDate = fields[PAYMENT_DATE]
                operation.paymentDate = paymentDate ? dateFormat.parse(paymentDate) : null

                def cardNumberTail = fields[CARD_NUMBER_TAIL]
                operation.cardNumberTail = cardNumberTail ? cardNumberTail[1..4] : null

                operation.state = TinkoffOperationState.valueOf(fields[STATE])

                operation.operationCurrency = Currency.getInstance(fields[OPERATION_CURRENCY])
                operation.operationValue = fields[OPERATION_VALUE].replace(",", ".").toBigDecimal()

                operation.paymentCurrency = Currency.getInstance(fields[PAYMENT_CURRENCY])
                operation.paymentValue = fields[PAYMENT_VALUE].replace(",", ".").toBigDecimal()

                def cashBackValue = fields[CASH_BACK_VALUE]
                operation.cashBackValue = cashBackValue ? cashBackValue.replace(",", ".").toBigDecimal() : null
                operation.bonusValue = fields[BONUS_VALUE].replace(",", ".").toBigDecimal()

                operation.category = fields[CATEGORY]
                def mcc = fields[MERCHANT_CATEGORY_CODE]
                operation.merchantCategoryCode = mcc ? mcc as Short : null
                operation.description = fields[DESCRIPTION]

                operations << operation

                ++linesRead
            }
        } catch (Exception e) {
            throw new TinkoffImportException("Can't import Tinkoff extraction data, line ${linesRead}", e)
        }

        return linesRead
    }
}
