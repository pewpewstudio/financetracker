package financetracker.bank.tinkoff

class TinkoffOperation {
    Date operationDate
    Date paymentDate

    String cardNumberTail

    TinkoffOperationState state

    Currency operationCurrency
    BigDecimal operationValue

    Currency paymentCurrency
    BigDecimal paymentValue

    BigDecimal cashBackValue
    BigDecimal bonusValue

    String category
    Short merchantCategoryCode
    String description
}
