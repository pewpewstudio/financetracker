package financetracker.bank

class DataImportException extends Exception {
    DataImportException(String message, Throwable cause) {
        super(message, cause)
    }

    DataImportException(String message) {
        super(message)
    }
}
