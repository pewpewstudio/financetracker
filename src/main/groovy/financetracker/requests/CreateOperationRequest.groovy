package financetracker.requests

import financetracker.Operation
import grails.validation.Validateable

class CreateOperationRequest implements Validateable {

    BigDecimal value
    String description

    static constraints = {
        importFrom Operation, inclue: ["value", "description"]
    }
}
