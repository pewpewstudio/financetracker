package financetracker.requests

import financetracker.User
import grails.validation.Validateable

class LoginRequest implements Validateable {

    String login
    String password

    static constraints = {
        importFrom User, inclue: ["login"]

        password nullable: false, blank: false, password: true
    }
}
