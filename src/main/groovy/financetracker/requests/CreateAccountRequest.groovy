package financetracker.requests

import financetracker.Account
import grails.databinding.BindUsing
import grails.validation.Validateable

class CreateAccountRequest implements Validateable {

    String name
    @BindUsing({ self, source ->
        def str = source["cardNumber"] as String
        return str?.replaceAll(/\s/, "")
    })
    String cardNumber
    @BindUsing({ self, source ->
        return source["baseSum"] ?: 0.0
    })
    BigDecimal baseSum

    static constraints = {
        importFrom Account, inclue: ["name", "cardNumber", "baseSum"]
    }
}
