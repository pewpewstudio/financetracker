package financetracker.datasets

class ProvidersCategories {
    static Map<String, List<String>> data = [
            "GRAFIT": ["Канцелярия"],
            "GetTaxi": ["Такси", "Gett"],
            "Uber": ["Такси", "Uber"],
            "SteamGames": ["Игры"],
            "STEAMGAMES.COM": ["Игры"],
            "YM     *YandexTaxi": ["Такси", "Яндекс.Такси"],
            "YM*YandexTaxi": ["Такси", "Яндекс.Такси"]
    ]
}
