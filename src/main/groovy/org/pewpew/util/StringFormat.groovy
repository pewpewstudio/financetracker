package org.pewpew.util

class StringFormat {

    static String truncateFormatted(String str, int padding, String placeholder) {
        int strSize = str.size()

        if (strSize > padding * 2 + placeholder.size()) {
            return "${str[0..padding - 1]}${placeholder}${str[strSize - padding..strSize - 1]}"
        } else {
            return str
        }
    }
}
