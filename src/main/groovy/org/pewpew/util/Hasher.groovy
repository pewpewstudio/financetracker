package org.pewpew.util

import java.security.MessageDigest

class Hasher {

    private Hasher() {}

    static String sha256(String val, String charsetName = "UTF-8") {
        def md = MessageDigest.getInstance("SHA-256")
        md.update(val.getBytes(charsetName))

        byte[] hash = md.digest()

        return hashToString(hash)
    }

    static String hashToString(byte[] hash) {
        def sb = new StringBuffer()

        for (i in hash) {
            String hex = Integer.toHexString(0xFF & i)

            if (hex.length() == 1) {
                sb.append('0')
            }

            sb.append(hex)
        }

        return sb.toString()
    }
}
