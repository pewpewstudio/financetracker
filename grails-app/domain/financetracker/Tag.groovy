package financetracker

class Tag {

    String name
    User user

    static hasMany = [operations: Operation]
    static belongsTo = Operation

    static mapping = {
        table "tags"
        version false

        id generator: "sequence", params: [sequence: "tags_id_seq"]
    }

    static constraints = {
        name nullable: false, blank: false, maxSize: 20
        user nullable: false
    }
}
