package financetracker

import java.text.SimpleDateFormat

class User {

    String login
    String passwordHash
    Date registered

    static hasMany = [accounts: Account, tags: Tag]

    static mapping = {
        table "users"
        version false

        id generator: "sequence", params: [sequence: "users_id_seq"]
    }

    static constraints = {
        login nullable: false, blank: false, maxSize: 30, unique: true
        passwordHash nullable: false, blank: false, size: 64..64
        registered nullable: false
    }

    String toString() {
        def formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss")

        return "$login, registered at: ${formatter.format(registered)}"
    }
}
