package financetracker

class Operation {

    Account account
    Date created
    BigDecimal value
    String description

    transient BigDecimal cumulativeValue

    static hasMany = [tags: Tag]
    static transients = ["cumulativeValue"]

    static mapping = {
        table "operations"
        version false

        id generator: "sequence", params: [sequence: "operations_id_seq"]
    }

    transient private final static BigDecimal limit = 99999999999999.99

    static constraints = {
        account nullable: false
        created nullable: false
        value nullable: false, max: limit, min: -limit, scale: 2
        description nullable: false, blank: true, maxSize: 400
    }
}
