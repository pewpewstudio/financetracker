package financetracker

class Account {

    User user
    String name
    String cardNumber
    BigDecimal baseSum

    static hasMany = [changes: Operation]

    static mapping = {
        table "accounts"
        version false

        id generator: "sequence", params: [sequence: "accounts_id_seq"]
    }

    transient private final static BigDecimal limit = 99999999999999.99

    static constraints = {
        user nullable: false
        name nullable: false, blank: false, maxSize: 40, unique: true
        cardNumber nullable: false, blank: false, creditCard: true
        baseSum nullable: false, max: limit, min: -limit, scale: 2, defaultValue: 0.0 as BigDecimal
    }

    String toString() {
        return "${name} (*${cardNumber[12..15]})"
    }
}
