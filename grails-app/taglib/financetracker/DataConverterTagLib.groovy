package financetracker

import grails.converters.JSON

class DataConverterTagLib {
    static namespace = "ft"

    static defaultEncodeAs = [taglib: 'raw']

    /**
     * Convert operations list to JSON object for Morris.js graph data.<br/>
     *
     * @emptyTag
     *
     * @attr list REQUIRED List of operations
     * @attr cumulative Cumulative data
     */
    def operationsToJSON = { attrs ->
        def operations = attrs["list"] as List<Operation>

        def mapped = operations.collect { [date: it.created, value: it.cumulativeValue] }

        out << (mapped as JSON).toString()
    }

    /**
     * Convert operations list to JSON object for Chartist.js graph data.<br/>
     *
     * @emptyTag
     *
     * @attr list REQUIRED List of operations
     * @attr cumulative Cumulative data
     */
    def operationsToSeparatedJSON = { attrs ->
        def operations = attrs["list"] as List<Operation>

        def labels = operations.collect { it.created.format("dd.MM.yyyy") }
        def series = [operations.collect { it.cumulativeValue }]

        out << ([labels: labels, series: series] as JSON).toString()
    }
}
