package financetracker

import org.grails.plugins.web.taglib.ValidationTagLib
import org.springframework.validation.FieldError

class ErrorsRenderTagLib {
    static namespace = "ft"

    static defaultEncodeAs = [taglib:'html']

    def style = { attrs ->
        def failed = attrs["failed"] ?: ""
        def success = attrs["success"] ?: ""

        def fieldName = attrs["field"]

        def validation = new ValidationTagLib()
        def errorsList = validation.extractErrors(attrs)
        if (errorsList) {
            validation.eachErrorInternalForList(attrs, errorsList, { FieldError it ->
                if (it.field == fieldName) {
                    out << failed
                    return
                }
            })
        }

        out << success
    }
}
