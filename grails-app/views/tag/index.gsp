<%@ page import="grails.converters.JSON" %>
<%@ page expressionCodec="none" %>
<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Редактирование счета</title>
    </head>

    <body>
        <content tag="header">
            Категории
        </content>

        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-tag fa-fw"></i> Мои категории
                    </div>

                    <div class="panel-body">
                        <g:each in="${tagsCounted}" var="tag">
                            <g:link controller="operation" action="tagged" params="[tagId: tag.tag.id]"
                                    class="btn btn-success btn" style="margin-bottom: 5px;">
                                ${tag.tag.name} (${tag.operations})
                            </g:link>
                        </g:each>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Рейтинг категорий
                    </div>

                    <div class="panel-body">
                        <div id="top-chart"></div>
                    </div>
                </div>
            </div>
        </div>

        <content tag="postscript">
            <script>
                $(() => {
                    let data = [];
                    data = ${chartData as JSON};

                    new Morris.Donut({
                        data: data,
                        element: "top-chart"
                    })
                });
            </script>
        </content>
    </body>
</html>