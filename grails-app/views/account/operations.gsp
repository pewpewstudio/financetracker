<html>
    <head>
        <meta name="layout" content="admin"/>
        <title>Редактирование счета</title>
    </head>

    <body>
        <content tag="header">
            ${account.name} <small>Операции над счетом</small>
        </content>

        <div class="row nav-pills-menu">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li>
                        <g:link controller="account" action="show" id="${account.id}">Обзор</g:link>
                    </li>
                    <li class="active">
                        <g:link controller="account" action="operations" id="${account.id}">Операции</g:link>
                    </li>
                    <li>
                        <g:link controller="account" action="importData" id="${account.id}">Импорт</g:link>
                    </li>
                </ul>
            </div>
        </div>

        <g:if test="${errorMessage}">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">${errorMessage}</div>
                </div>
            </div>
        </g:if>

        <g:if test="${successMessage}">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">${successMessage}</div>
                </div>
            </div>
        </g:if>

        <div class="row">
            <div class="col-md-8 col-sm-6">
                <g:form name="create_operation" controller="account" action="createOperation" id="${account.id}" useToken="true">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Добавление данных
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="value">Сумма</label>

                                <div class="input-group">
                                    <div class="input-group-addon">&#8381;</div>
                                    <input type="text" id="value" name="value" class="form-control" value=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description">Описание</label>
                                <input type="text" id="description" name="description" class="form-control" value=""/>
                            </div>

                            <div class="form-group">
                                <label for="tags">Категории</label>
                                <g:select id="tags" name="tags[]" from="${tags}" optionValue="name" optionKey="name" noSelection="['':'']" multiple="true" class="form-control"/>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-xs">Добавить</button>
                        </div>
                    </div>
                </g:form>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bars fa-fw"></i> Недавние операции
                    </div>

                    <div class="panel-body">
                        <g:if test="${operations}">
                            <div class="list-group">
                                <g:each in="${operations}" var="operation">
                                    <a href="#" class="list-group-item">
                                        <g:if test="${operation.value > 0}">
                                            <i class="fa fa-plus fa-fw text-success"></i> ${operation.value}
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-minus fa-fw text-primary"></i> ${operation.value}
                                        </g:else>
                                        <span class="pull-right text-muted small"><em>${operation.created.format("dd.MM hh:mm:ss")}</em></span>
                                    </a>
                                </g:each>
                            </div>

                            <g:link controller="operation" action="all" id="${account.id}" class="btn btn-default btn-block">
                                Все операции
                            </g:link>
                        </g:if>
                        <g:else>
                            <div class="text-center text-muted">
                                Нет данных об операциях
                            </div>
                        </g:else>
                    </div>
                </div>
            </div>
        </div>

        <content tag="postscript">
            <script>
                $(() => {
                    $("#tags").select2({
                        tags: true,
                        tokenSeparators: [',']
                    })
                });
            </script>
        </content>
    </body>
</html>