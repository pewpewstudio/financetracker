<%@ page import="grails.converters.JSON" %>
<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Обзор счета</title>
    </head>

    <body>
        <content tag="header">
            ${account.name} <small>Обзор</small>
        </content>

        <div class="row nav-pills-menu">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li class="active">
                        <g:link controller="account" action="show" id="${account.id}">Обзор</g:link>
                    </li>
                    <li>
                        <g:link controller="account" action="operations" id="${account.id}">Операции</g:link>
                    </li>
                    <li>
                        <g:link controller="account" action="importData" id="${account.id}">Импорт</g:link>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        График движения средств
                    </div>

                    <div class="panel-body">
                        <div id="morris-area-chart"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-money fa-fw"></i> Баланс
                    </div>

                    <div class="panel-body">
                        ${balance}
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bars fa-fw"></i> Размер операций за период
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <b>Пополнение:</b>
                            </div>

                            <div class="col-md-9">
                                ${operationsSum.positive}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <b>Расход:</b>
                            </div>

                            <div class="col-md-9">
                                ${operationsSum.negative}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bars fa-fw"></i> Недавние операции (всего ${totalOperations})
                    </div>

                    <div class="panel-body">
                        <g:if test="${recentOperations}">
                            <div class="list-group">
                                <g:each in="${recentOperations}" var="operation">
                                    <a href="#" class="list-group-item">
                                        <g:if test="${operation.value > 0}">
                                            <i class="fa fa-plus fa-fw text-success"></i> ${operation.value}
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-minus fa-fw text-primary"></i> ${operation.value}
                                        </g:else>
                                        ${operation.description}
                                        <span class="pull-right text-muted small"><em>${operation.created.format("dd.MM hh:mm:ss")}</em>
                                        </span>
                                    </a>
                                </g:each>
                            </div>

                            <g:link controller="operation" action="all" id="${account.id}"
                                    class="btn btn-default btn-block">
                                Все операции
                            </g:link>
                        </g:if>
                        <g:else>
                            <div class="text-center text-muted">
                                Нет данных об операциях
                            </div>
                        </g:else>
                    </div>
                </div>
            </div>
        </div>

        <content tag="postscript">
            <script>
                let data = {};
                data = ${ft.operationsToSeparatedJSON(list: chartData)};

                $(() => {
                    new Chartist.Line('#morris-area-chart', data, {
                        fullWidth: true,
                        height: '500px',
                        showPoint: false,
                        axisX: {showGrid: false, showLabel: false}
                    })
                })
            </script>
        </content>
    </body>
</html>