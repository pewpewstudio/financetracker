<%@ page import="grails.converters.JSON" %>
<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Обзор счета</title>
    </head>

    <body>
        <content tag="header">
            ${account.name} <small>Импорт данных</small>
        </content>

        <div class="row nav-pills-menu">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li>
                        <g:link controller="account" action="show" id="${account.id}">Обзор</g:link>
                    </li>
                    <li>
                        <g:link controller="account" action="operations" id="${account.id}">Операции</g:link>
                    </li>
                    <li class="active">
                        <g:link controller="account" action="importData" id="${account.id}">Импорт</g:link>
                    </li>
                </ul>
            </div>
        </div>

        <g:if test="${errorMessage}">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">${errorMessage}</div>
                </div>
            </div>
        </g:if>

        <g:if test="${successMessage}">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">${successMessage}</div>
                </div>
            </div>
        </g:if>

        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Импорт данных
                    </div>

                    <div class="panel-body">
                        <g:uploadForm name="dataFileUpload" action="importFromFile" useToken="true" id="${account.id}">
                            <div class="form-group">
                                <label for="dataFile">Файл с данными</label>
                                <input type="file" id="dataFile" name="dataFile" accept="text/csv,application/vnd.ms-excel" />
                            </div>
                            <button type="submit" class="btn btn-primary">Загрузить</button>
                        </g:uploadForm>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>