<html>
    <head>
        <meta name="layout" content="admin"/>
        <title>Новый счет</title>
    </head>

    <body>
        <content tag="header">
            Создание нового счета
        </content>

        <div class="row nav-pills-menu">
            <div class="col-md-12">
                <g:form controller="account" action="save">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Параметры счета
                        </div>

                        <div class="panel-body">
                            <g:if test="${errorMessage}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">${errorMessage}</div>
                                    </div>
                                </div>
                            </g:if>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Название</label>
                                        <input type="text" id="name" name="name" class="form-control" value="${oldData?.name}"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="base-sum">Начальная сумма</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">&#8381;</div>
                                            <input type="text" id="base-sum" name="baseSum" class="form-control" value="${oldData?.baseSum}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Параметры карты</label>

                                    <div id="card-data" style="max-width: 400px;"></div>
                                </div>
                            </div>

                        </div>

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary btn-xs">Сохранить</button>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>

        <content tag="postscript">
            <script>
                $(() => {
                    let card = new PaymentCard();
                    $("#card-data").append(card.dom());
                    $("input[name='cardNumber']").inputmask({
                        mask: "9999 9999 9999 9999",
                        placeholder: " ",
                        showMaskOnHover: false,
                        showMaskOnFocus: false
                    });
                });
            </script>
        </content>
    </body>
</html>