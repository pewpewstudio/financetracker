<%@ page import="financetracker.User" %>
<html>
    <head>
        <meta name="layout" content="main"/>

        <title>Home</title>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Finance Tracker</a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <g:set var="user" value="${session["user"] as User}"/>

                        <g:if test="${user}">
                            <li><g:link controller="dashboard">Личный кабинет</g:link></li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    ${user.login} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><g:link controller="auth" action="logout">Выход</g:link></li>
                                </ul>
                            </li>
                        </g:if>
                        <g:else>
                            <li><g:link controller="auth" action="register">Регистрация</g:link></li>
                            <li><g:link controller="auth" action="login">Вход</g:link></li>
                        </g:else>
                    </ul>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">
                <h1>Finance Tracker</h1>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>Heading</h2>

                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                </div>

                <div class="col-md-4">
                    <h2>Heading</h2>

                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                </div>

                <div class="col-md-4">
                    <h2>Heading</h2>

                    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                </div>
            </div>

            <hr>

            <footer>
                <p>&copy; 2017 PEW-PEW</p>
            </footer>
        </div>

        <asset:javascript src="jquery/jquery-3.1.1.js"/>
        <asset:javascript src="bootstrap/javascripts/bootstrap/collapse.js"/>
    </body>
</html>
