<%@ page import="org.springframework.validation.FieldError" %>
<html>
    <head>
        <meta name="layout" content="main"/>

        <title>Регистрация</title>

        <asset:stylesheet src="widgets/login-form"/>
    </head>

    <body>
        <div class="container">
            <div class="text-left">
                <a href="/" class="btn btn-xl"><i class="fa fa-home fa-fw"></i> На главную</a>
            </div>

            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Регистрация</h3>

                                <p>Заполните поля</p>
                            </div>

                            <div class="form-top-right">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>

                        <div class="form-bottom">
                            <g:form name="register_form" action="create" class="registration-form">
                                <g:hasErrors bean="${validationError}">
                                    <div class="error-notification">Некоторые поля заполнены некорректно</div>
                                </g:hasErrors>

                                <div class="form-group">
                                    <g:set var="style" value="${ft.style(bean: validationError, field: 'login', failed: 'input-error')}"/>
                                    <input type="text" name="login" placeholder="Логин" class="form-first-name form-control ${style}" value="${oldData?.login}">
                                </div>

                                <div class="form-group">
                                    <g:set var="style" value="${ft.style(bean: validationError, field: 'password', failed: 'input-error')}"/>
                                    <input type="password" name="password" placeholder="Пароль" class="form-first-name form-control ${style}">
                                </div>

                                <button type="submit" class="btn">Sign in!</button>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>