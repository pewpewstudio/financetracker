<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Все операции в категории ${tag.name}</title>
    </head>

    <body>
        <content tag="header">
            Все операции по над счетом
        </content>

        <div class="row">
            <div class="col-md-12">
                <g:if test="${operations}">
                    <div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Сумма</th>
                                        <th>Счет</th>
                                        <th>Описание</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <g:each in="${operations}" var="operation">
                                        <tr>
                                            <td>${operation.created.format("dd.MM.yyyy HH:mm:ss")}</td>
                                            <td>${operation.value}</td>
                                            <td>${operation.account}</td>
                                            <td>${operation.description ?: '-'}</td>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>
                        </div>

                        <div>
                            <g:if test="${count > max}">
                                <bs:paginate controller="operation" action="tagged" total="${count}" params="[tagId: tag.id]" />
                            </g:if>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <p class="text-muted">В данной категории нет записей</p>
                </g:else>
            </div>
        </div>
    </body>
</html>