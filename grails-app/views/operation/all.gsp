<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Все операции над счетом</title>
    </head>

    <body>
        <content tag="header">
            Все операции по над счетом
        </content>

        <div class="row">
            <div class="col-md-12">
                <g:if test="${operations}">
                    <div>
                        <div class="table-responsive">
                            <table class="table">
                                <colgroup>
                                    <col />
                                    <col />
                                    <col />
                                    <col width="45%" />
                                    <col width="25%" />
                                </colgroup>

                                <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Сумма</th>
                                        <th>Счет</th>
                                        <th>Описание</th>
                                        <th>Категории</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <g:each in="${operations}" var="operation">
                                        <tr>
                                            <td>${operation.created.format("dd.MM.yyyy HH:mm:ss")}</td>
                                            <td>${operation.value}</td>
                                            <td>${operation.account}</td>
                                            <td>${operation.description ?: '-'}</td>
                                            <td>
                                                <g:each in="${operation.tags}" var="tag">
                                                    <g:link controller="operation" action="tagged" params="${[tagId: tag.id]}"
                                                            class="label label-info">
                                                        ${tag.name}
                                                    </g:link>
                                                </g:each>
                                            </td>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <p class="text-muted">В данной категории нет записей</p>
                </g:else>
            </div>
        </div>
    </body>
</html>