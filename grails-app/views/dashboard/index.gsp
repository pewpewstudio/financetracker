<%@ page import="financetracker.Account" %>
<html>
    <head>
        <meta name="layout" content="admin" />
        <title>Главная</title>
    </head>

    <body>
        <content tag="header">
            Главная
        </content>

        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-dashboard fa-fw"></i> Быстрый доступ
                    </div>

                    <div class="panel-body">
                        <h4>Мои счета</h4>

                        <div>
                            <g:each in="${accounts}" var="account">
                                <g:link controller="account" action="show" id="${account.id}"
                                        class="btn btn-primary btn-xs btn-outline">
                                    ${account.name}
                                </g:link>
                            </g:each>
                            <span>
                                <g:link controller="account" action="create" class="btn btn-default btn-xs">
                                    <i>создать...</i>
                                </g:link>
                            </span>
                        </div>

                        <hr/>

                        <h4>Мои категории</h4>

                        <div>
                            <g:each in="${tagsCounted}" var="tag">
                                <g:link controller="operation" action="tagged" params="[tagId: tag.tag.id]"
                                        class="btn btn-success btn-xs btn-outline" style="margin-bottom: 5px;">
                                    ${tag.tag.name} (${tag.operations})
                                </g:link>
                            </g:each>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bars fa-fw"></i> Недавние операции
                    </div>

                    <div class="panel-body">
                        <g:if test="${recentOperations}">
                            <div class="list-group">
                                <g:each in="${recentOperations}" var="operation">
                                    <g:set var="account" value="${operation.account as Account}"/>
                                    <g:link controller="operation" action="all" id="${account.id}" class="list-group-item">
                                        <g:if test="${operation.value > 0.0}">
                                            Пополнение счета
                                        </g:if>
                                        <g:else>
                                            Списание со счета
                                        </g:else>
                                        <b>${account.name}</b>
                                        на сумму ${operation.value}
                                    </g:link>
                                </g:each>
                            </div>
                        </g:if>
                        <g:else>
                            <div class="text-center text-muted">
                                Нет данных об операциях
                            </div>
                        </g:else>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>