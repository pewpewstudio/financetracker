<!DOCTYPE html>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content="Good finance tracker"/>
        <meta name="author" content="NB, RK, NS, JS, VM, JI"/>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i&amp;subset=cyrillic" rel="stylesheet">
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" href="/assets/favicon-32x32.png" sizes="32x32"/>
        <link rel="icon" type="image/png" href="/assets/favicon-16x16.png" sizes="16x16"/>
        <link rel="manifest" href="/assets/manifest.json"/>
        <link rel="mask-icon" href="/assets/safari-pinned-tab.svg"/>
        <meta name="apple-mobile-web-app-title" content="Finance Tracker"/>
        <meta name="application-name" content="Finance Tracker"/>
        <meta name="msapplication-config" content="/assets/browserconfig.xml"/>
        <meta name="theme-color" content="#ffffff"/>

        <title>Finance Tracker | <g:layoutTitle default="Void"/></title>

        <asset:stylesheet src="home-page"/>

        <g:layoutHead/>
    </head>

    <body>
        <g:layoutBody/>
    </body>
</html>
