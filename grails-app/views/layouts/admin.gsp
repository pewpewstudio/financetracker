<!DOCTYPE html>

<%@ page import="financetracker.User; financetracker.Account" %>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content="Good finance tracker"/>
        <meta name="author" content="NB, RK, NS, JS, VM, JI"/>

        <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" href="/assets/favicon-32x32.png" sizes="32x32"/>
        <link rel="icon" type="image/png" href="/assets/favicon-16x16.png" sizes="16x16"/>
        <link rel="manifest" href="/assets/manifest.json"/>
        <link rel="mask-icon" href="/assets/safari-pinned-tab.svg"/>
        <meta name="apple-mobile-web-app-title" content="Finance Tracker"/>
        <meta name="application-name" content="Finance Tracker"/>
        <meta name="msapplication-config" content="/assets/browserconfig.xml"/>
        <meta name="theme-color" content="#ffffff"/>

        <title>Finance Tracker | <g:layoutTitle default="Void"/></title>

        <asset:stylesheet src="dashboard"/>

        <g:layoutHead/>
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <asset:image src="ftracker_logo@83x48.png" width="83" height="48" alt="Finance Tracker"/>
                    </a>
                </div>

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a id="user-profile-menu" class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> ${session["user"].login} <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <g:link controller="auth" action="logout"><i class="fa fa-sign-out fa-fw"></i> Выход</g:link>
                            </li>
                        </ul>
                    </li>
                </ul>

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <g:link controller="dashboard" action="index">
                                    <i class="fa fa-home fa-fw"></i> Главная
                                </g:link>
                            </li>

                            <li class="active">
                                <a href="#">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Мои счета
                                </a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <g:link elementId="add-account" controller="account" action="create">
                                            <i class="fa fa-plus fa-fw"></i> Добавить
                                        </g:link>
                                    </li>
                                    <g:each in="${Account.findAllByUser(session["user"] as User)}" var="account">
                                        <li>
                                            <g:link controller="account" action="show" id="${account.id}">${account.name}</g:link>
                                        </li>
                                    </g:each>
                                </ul>
                            </li>

                            <li>
                                <g:link controller="tag" action="index">
                                    <i class="fa fa-tags fa-fw"></i> Категории
                                </g:link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"><g:pageProperty name="page.header"/></h1>
                    </div>
                </div>

                <div>
                    <g:layoutBody/>
                </div>
            </div>
        </div>

        <asset:javascript src="application"/>
        <g:pageProperty name="page.postscript"/>
    </body>
</html>
