<!doctype html>
<html>
    <head>
        <title>404</title>
        <meta name="layout" content="main">
        <style>
        body {
            background: url('${assetPath(src: '404bk.jpg')}') no-repeat center center fixed;
            background-size: cover;
            color: white;
        }

        h1 {
            font-size: 8em;
        }

        #container {
            width: 95%;
            margin: 0 auto;
            text-align: center;
            padding: 3% 0;
            clear: both;
            position: relative;
            top: 50%;
        }

        #text {
            padding: 10% 0;
        }

        #tag {
            display: block;
            font-size: 1.2em;
            margin-top: 1%;
            font-weight: bold;
            clear: both;
        }

        #tag > a {
            color: white;
        }

        #line {
            height: 0.6em;
            background-color: #000;
            opacity: 0.2;
            display: block;
            max-width: 53em;
            margin: 0 auto;
            border: none;
        }
        </style>
    </head>

    <body>
        <div id="container">
            <div id="text">
                <h1>404</h1>
                <hr id="line" />
                <span id="tag">
                    <a href="/"><i class="fa fa-home fa-fw"></i> Back home</a>
                </span>
            </div>
        </div>
    </body>
</html>
