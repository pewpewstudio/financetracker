package financetracker


class AccountInterceptor {

    def accountService

    AccountInterceptor() {
        match(controller: "account")
                .excludes(action: "create")
                .excludes(action: "save")
    }

    boolean before() {
        def user = session["user"] as User

        if (!user) {
            redirect(uri: "/")
            return false
        }

        if (accountService.checkAccountOwning(user, params.int("id"))) {
            return true
        } else {
            render(status: 404)
            return false
        }
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
