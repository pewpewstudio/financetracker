package financetracker

import financetracker.requests.LoginRequest
import financetracker.requests.RegisterRequest
import grails.validation.ValidationException

class AuthController {

    def authService

    static defaultAction = "login"

    def login() {
        [:]
    }

    def authenticate(LoginRequest params) {
        if (params.validate()) {
            def user = authService.findUserByLogin(params.login)

            if (user && authService.checkPassword(user, params.password)) {
                session["user"] = user
                return redirect(controller: "dashboard")
            } else {
                def message = "Комбинация логина и пароля не найдена"
                return chain(action: "login", model: [errorMessage: message, oldData: params])
            }
        } else {
            return chain(action: "login", model: [validationError: params.errors, oldData: params])
        }
    }

    def register() {
        [:]
    }

    def create(RegisterRequest params) {
        if (params.validate()) {
            def user = authService.createUser(params.login, params.password)
            session["user"] = user

            return redirect(controller: "dashboard")
        } else {
            return chain(action: "register", model: [validationError: params.errors, oldData: params])
        }
    }

    def logout() {
        session["user"] = null
        return redirect(uri: "/")
    }
}