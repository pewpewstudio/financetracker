package financetracker

import financetracker.bank.DataImportException
import financetracker.requests.CreateAccountRequest
import financetracker.requests.CreateOperationRequest
import org.springframework.core.io.InputStreamSource

class AccountController {

    def accountService
    def importService
    def operationService
    def tagService

    def create() {
        [:]
    }

    def save(CreateAccountRequest params) {
        if (params.validate()) {
            def user = session["user"] as User
            def account = accountService.createAccount(user, params.name, params.cardNumber, params.baseSum)

            if (account) {
                redirect(action: "show", id: account.id)
            } else {
                def message = "Счет с таким названием уже существует"
                chain(action: "create", model: [errorMessage: message, oldData: params])
            }
        } else {
            log.info(params.errors.allErrors.toString())
            def message = "Форма заполнена некорректно"
            chain(action: "create", model: [errorMessage: message, validationError: params.errors, oldData: params])
        }
    }

    def show(Integer id) {
        def user = session["user"] as User

        def account = accountService.readAccountForUser(user, id)

        if (!account) {
            return render(status: 404)
        }

        def recentOperations = operationService.getRecentOperations(account)
        def totalOperations = operationService.getTotalOperation(account)
        def operationsSum = operationService.getOperationsSum(account)
        def chartData = operationService.getOperationsForChart(account)

        def balance = operationsSum.sum + account.baseSum

        [account: account, recentOperations: recentOperations, totalOperations: totalOperations,
         operationsSum: operationsSum, chartData: chartData, balance: balance]
    }

    def operations(Integer id) {
        def user = session["user"] as User

        def account = accountService.readAccountForUser(user, id)

        if (!account) {
            return render(status: 404)
        }

        def operations = operationService.getRecentOperations(account)
        def tags = tagService.getUserTags(user)

        [account: account, operations: operations, tags: tags]
    }

    def createOperation(Integer id, CreateOperationRequest validatedParams) {
        def user = session["user"] as User

        def account = accountService.readAccountForUser(user, id)

        if (!account) {
            return render(status: 404)
        }

        if (validatedParams.validate()) {
            withForm {
                def tagsNames = params.list("tags[]")

                def tags = tagService.mergeTags(user, tagsNames)

                // TODO: make service
                def operation = new Operation()
                operation.account = account
                operation.created = new Date()
                operation.value = validatedParams.value
                operation.description = validatedParams.description

                operation.tags = tags

                operation.save(flush: true)

                def message = "Операция добавлена"
                chain(action: "operations", id: id, model: [successMessage: message])
            }.invalidToken {
                def message = "Ошибка: создание дубликата"
                chain(action: "operations", id: id, model: [errorMessage: message])
            }
        } else {
            def message = "Форма заполнена некорректно"
            chain(action: "operations", id: id, model: [errorMessage: message, validationError: validatedParams.errors])
        }
    }

    def importData(Integer id) {
        def account = Account.get(id)

        [account: account, successMessage: flash.successMessage, errorMessage: flash.errorMessage]
    }

    def importFromFile(Integer id) {
        def file = request.getFile("dataFile")

        if (file.empty) {
            flash.errorMessage = "Файл не задан"
        } else {
            try {
                int operations = importService.importTinkoffCsv(Account.get(id), file as InputStreamSource)

                flash.successMessage = "Успешно импортировано $operations операций"
            } catch (DataImportException e) {
                log.error("Data import error", e)
                flash.errorMessage = "Имопртируемый файл не соответствует формату или поврежден"
            }
        }

        redirect(action: "importData", id: id)
    }
}
