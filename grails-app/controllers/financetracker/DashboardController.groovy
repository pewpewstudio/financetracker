package financetracker

class DashboardController {

    def tagService
    def operationService

    def index() {
        def user = session["user"] as User

        def accounts = Account.findAllByUser(user)

        def tags = tagService.getUserTags(user)

        def tagsCounted = tags.collect {
            [tag: it, operations: it.operations.size()]
        }.findAll {it.operations > 0}

        def recentOperations = operationService.getRecentOperationsByUser(user)

        [accounts: accounts, tagsCounted: tagsCounted, recentOperations: recentOperations]
    }
}
