package financetracker

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class RequestLogInterceptor {

    static Logger log = LoggerFactory.getLogger("Access")

    RequestLogInterceptor() {
        matchAll()
    }

    boolean before() {

        def requestParams = params.clone()

        requestParams.remove("controller")
        requestParams.remove("action")

        if (requestParams["password"]) {
            requestParams["password"] = "******"
        }

        def loginString = "not authenticated"

        def user = session["user"] as User

        if (user) {
            loginString = "authenticated as ${user.login}"
        }

        log.info("Request ${controllerName}/${actionName}, params: $requestParams, ${loginString}, ip: ${request.remoteHost}")

        return true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
