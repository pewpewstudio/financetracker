package financetracker

class TagController {

    def tagService

    def index() {
        def user = session["user"] as User

        def tags = tagService.getUserTags(user)

        def tagsCounted = tags.collect {
            [tag: it, operations: it.operations.size()]
        }.findAll {it.operations > 0}

        def chartData = tagsCounted.sort(false) { a, b -> b.operations <=> a.operations }.take(10).collect {
            [label: it.tag.name, value: it.operations]
        }

        [tagsCounted: tagsCounted, chartData: chartData]
    }
}
