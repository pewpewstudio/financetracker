package financetracker

import grails.orm.PagedResultList

class OperationController {

    def operationService

    def show(Integer id) {

    }

    def all(Integer id) {
        def operations = operationService.getOperations(Account.get(id))

        [operations: operations]
    }

    def tagged(Integer tagId) {
        final int MAX_OPERATIONS_DEFAULT = 50

        int max = params["max"] ? params.int("max") : MAX_OPERATIONS_DEFAULT
        params["max"] = max

        def user = session["user"] as User
        def offset = params["offset"] ? params.int("offset") : 1

        def tag = Tag.read(tagId)

        def operations = operationService.getOperationsByTag(user, tag, max, offset) as PagedResultList

        int count = operations.totalCount

        [operations: operations, tag: tag, count: count, max: max]
    }
}
