package financetracker


class AuthInterceptor {

    AuthInterceptor() {
        matchAll()
    }

    boolean before() {
        def user = session["user"] as User

        if (restricted) {
            if (user) {
                return true
            } else {
                redirect(controller: "auth", action: "login")
                return false
            }
        }

        return true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

    boolean isRestricted() {
        def findEntry = restrictedActions.find {it.key == controllerName}

        if (findEntry) {
            if (findEntry.value) {
                if (actionName in findEntry.value) {
                    return true
                }
            } else {
                return true
            }
        }

        return false
    }

    def getRestrictedActions() {
        def list = [
                "dashboard": [],
                "auth": ["logout"],
                "account": [],
                "tag": [],
                "operation": []
        ]

        return list
    }
}
