import grails.util.BuildSettings
import grails.util.Environment

def targetDir = BuildSettings.TARGET_DIR

appender("ACCESS", RollingFileAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d %msg%n"
    }

    rollingPolicy(TimeBasedRollingPolicy) {
        FileNamePattern = "${targetDir}/%d{yyyy-MM-dd}-access.log.zip"
    }
}

appender("ACCESS_STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d %msg%n"
    }
}

appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
}


if (Environment.isDevelopmentMode() && targetDir != null) {
    appender("FULL_STACKTRACE", FileAppender) {
        file = "${targetDir}/stacktrace.log"
        append = true
        encoder(PatternLayoutEncoder) {
            pattern = "%level %logger - %msg%n"
        }
    }
    logger("StackTrace", ERROR, ['FULL_STACKTRACE'], false)
    root(ERROR, ['STDOUT', 'FULL_STACKTRACE'])
}
else {
    root(ERROR, ['STDOUT'])
}

def appNamespaces = [
        "grails.app.controllers",
        "grails.app.services"
]

appNamespaces.each {
    logger(it, DEBUG, ["STDOUT"], false)
}

logger("Access", INFO, ["ACCESS", "ACCESS_STDOUT"], false)