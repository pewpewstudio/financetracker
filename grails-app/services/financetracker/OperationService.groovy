package financetracker

import org.grails.orm.hibernate.query.AbstractHibernateCriteriaBuilder as CB

class OperationService {

    def getOperationsSum(Account account, Range<Date> period = null) {
        BigDecimal positive = Operation.withCriteria {
            gt("value", 0.0)
            sumCriteria(delegate as CB, account, period)
        }[0] ?: 0.0

        BigDecimal negative = Operation.withCriteria {
            lt("value", 0.0)
            sumCriteria(delegate as CB, account, period)
        }[0] ?: 0.0

        return [positive: positive, negative: negative, sum: positive + negative]
    }
    def getOperations(Account account, Range<Date> period = null) {
        def operations = Operation.withCriteria {
            periodCriteria(delegate as CB, account, period)
            order("created", "desc")
        } as List<Operation>

        return operations
    }

    def getRecentOperations(Account account, Integer max = 10) {
        def operations = Operation.findAllByAccount(account, [max: max, sort: "created", order: "desc"])

        return operations
    }

    def getRecentOperationsByUser(User user, Integer max = 10) {
        def operations = Operation.withCriteria(sort: "created", order: "desc") {
            account {
                eq("user", user)
            }

            maxResults(max)
        }

        return operations
    }

    def getTotalOperation(Account account) {
        def count = Operation.countByAccount(account)

        return count
    }

    def getOperationsForChart(Account account, Range<Date> period = null) {
        def operations = getOperations(account, period)

        if (operations) {
            operations.reverse(true)

            operations[0].value += account.baseSum

            BigDecimal lastOperation = 0.0

            operations.each { operation ->
                operation.cumulativeValue = operation.value + lastOperation
                lastOperation = operation.cumulativeValue
            }

            return operations
        } else {
            return []
        }
    }

    def getOperationsByTag(User accountsUser, Tag tag, int max, int offset) {
        def criteria = Operation.createCriteria()

        def results = criteria.list(max: max, offset: offset) {
            account {
                eq("user", accountsUser)
            }

            tags {
                eq("id", tag.id)
            }
        }

        return results
    }

    private static void periodCriteria(CB cb, Account account, Range<Date> period = null) {
        cb.eq("account", account)
        if (period) {
            cb.between("created", period.from, period.to)
        }
    }

    private static void sumCriteria(CB cb, Account account, Range<Date> period = null) {
        periodCriteria(cb, account, period)

        cb.projections {
            cb.sum("value")
        }
    }
}
