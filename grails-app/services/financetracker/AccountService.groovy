package financetracker

import grails.transaction.Transactional

class AccountService {

    @Transactional
    Account createAccount(User user, String name, String cardNumber, BigDecimal baseSum) {
        if (Account.findByUserAndName(user, name)) {
            return null
        }

        Account account = new Account(user: user, name: name, cardNumber: cardNumber, baseSum: baseSum)

        account.save()

        return account
    }

    Account readAccountForUser(User user, Integer id) {
        def account = Account.where {
            id == id
            user == user
        }.get()

        return account
    }

    boolean checkAccountOwning(User user, Integer id) {
        def account = Account.where {
            id == id
            user == user
        }.get()

        return account ? true : false
    }
}
