package financetracker

import financetracker.bank.tinkoff.TinkoffImporter
import financetracker.bank.tinkoff.TinkoffOperationState
import financetracker.datasets.MerchantCategoryCodes
import financetracker.datasets.ProvidersCategories
import grails.transaction.Transactional
import org.springframework.core.io.InputStreamSource

@Transactional
class ImportService {

    def tagService

    int importTinkoffCsv(Account account, InputStreamSource stream) {
        TinkoffImporter importer = new TinkoffImporter()

        int operations = importer.read(stream)

        importer.operations.forEach {
            if (it.state == TinkoffOperationState.OK && (it.paymentDate || it.operationDate)) {
                def operation = new Operation()
                operation.account = account
                operation.created = it.operationDate
                operation.description = it.description
                operation.value = it.paymentValue

                def tags = []

                def mccTag = MerchantCategoryCodes.codes[it.merchantCategoryCode as Integer]

                if (mccTag) {
                    tags << mccTag
                }

                def providerTag = ProvidersCategories.data[it.description]

                if (providerTag) {
                    for (i in providerTag) {
                        tags << i
                    }
                }

                operation.tags = tagService.mergeTags(account.user, tags)

                operation.save()
            }
        }

        return operations
    }
}
