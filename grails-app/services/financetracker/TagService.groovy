package financetracker

import grails.transaction.Transactional

class TagService {

    def getUserTags(User user) {
        return Tag.findAllByUser(user, [sort: "name", order: "asc"])
    }

    @Transactional
    def mergeTags(User user, List<String> tagsNames) {
        def tags = []

        for (i in tagsNames) {
            tags << Tag.findOrSaveWhere(user: user, name: i)
        }

        return tags
    }
}
