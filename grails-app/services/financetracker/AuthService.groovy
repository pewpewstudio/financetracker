package financetracker

import grails.transaction.Transactional
import org.pewpew.util.Hasher
import financetracker.datasets.BasicUserTags

import java.text.SimpleDateFormat

class AuthService {

    private static SimpleDateFormat saltDateFormat

    AuthService() {
        saltDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    }

    def hashPassword(String password, String salt) {
        def saltedPassword = password + salt

        return Hasher.sha256(saltedPassword)
    }

    @Transactional
    def createUser(String login, String password) {
        Date registered = new Date()
        String hashedPassword = hashPassword(password, saltDateFormat.format(registered))

        User user = new User(login: login, passwordHash: hashedPassword, registered: registered, isActive: true)

        BasicUserTags.list.each {
            user.addToTags(new Tag(name: it))
        }

        user.save(flush: true)

        return user
    }

    def findUserByLogin(String login) {
        return User.findByLogin(login)
    }

    boolean checkPassword(User user, String password) {
        def passwordHash = hashPassword(password, saltDateFormat.format(user.registered))

        if (user.passwordHash == passwordHash) {
            return true
        }

        return false
    }
}
