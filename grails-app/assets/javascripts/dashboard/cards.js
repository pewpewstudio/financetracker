class PaymentCard {
    static get paymentSystemAssets() {
        return {
            visa: "system-visa",
            mastercard: "system-mastercard",
            maestro: "system-maestro"
        }

    };

    static get banksAssets() {
        return {
            tinkoff: "bank-tinkoff",
            sberbank: "bank-sberbank",
            raiffeisen: "bank-raiffeisen"
        }
    };

    constructor() {
        this.$cardBaseWrapper = $("<div class='card-base-wrapper'></div>");
        this.$cardBase = $("<div class='card-base'></div>");
        this.$cardBrandWrapper = $("<div class='card-brand-wrapper'></div>");
        this.$cardBrand = $("<div class='card-brand'></div>");
        let $cardNumberForm = $("<div class='card-number-form'></div>");

        $cardNumberForm.html(
            "<span>" +
            "   <input type='text' name='cardNumber' class='card-number' placeholder='0000 0000 0000 0000'/>" +
            "</span>");

        $cardNumberForm.find(".card-number").on('input', event => {
            this.update($(event.currentTarget));
        });

        this.$cardBrandWrapper.append(this.$cardBrand);
        this.$cardBase.append(this.$cardBrandWrapper);
        this.$cardBase.append($cardNumberForm);
        this.$cardBaseWrapper.append(this.$cardBase);
    }

    static detectBankAndPaymentSystem(str) {
        const BIN_LENGTH = 6;

        if (str.length >= BIN_LENGTH) {
            let paymentSystemCode = str[0];
            let bin = str.substr(0, BIN_LENGTH);

            let paymentSystem;

            if (paymentSystemCode == "4") {
                paymentSystem = this.paymentSystemAssets.visa;
            } else if (paymentSystemCode == "5") {
                paymentSystem = this.paymentSystemAssets.mastercard;
            } else if (paymentSystemCode == "6") {
                paymentSystem = this.paymentSystemAssets.maestro;
            } else {
                paymentSystem = null;
            }

            let bank;

            if (["521324", "437773"].indexOf(bin) != -1) {
                bank = this.banksAssets.tinkoff;
            } else if (["481776", "548401", "546955"].indexOf(bin) != -1) {
                bank = this.banksAssets.sberbank;
            } else if (["510069"].indexOf(bin) != -1) {
                bank = this.banksAssets.raiffeisen;
            } else {
                bank = null;
            }

            return {paymentSystem: paymentSystem, bank: bank};
        } else {
            return null;
        }
    }

    update(element) {
        let cardNumber = element.val().replace(/\s/g, "");

        let branded = false;
        let classes = ["card-base"];

        let pair = this.constructor.detectBankAndPaymentSystem(cardNumber);

        if (pair && pair.paymentSystem) {
            classes.push(pair.paymentSystem);
            branded = true;
        }

        if (pair && pair.bank) {
            classes.push(pair.bank);
            branded = true;
        }

        if (branded) {
            classes.push("card-branded");
        }

        this.$cardBase.attr("class", classes.join(" "));
    }

    dom() {
        return this.$cardBaseWrapper;
    }
}