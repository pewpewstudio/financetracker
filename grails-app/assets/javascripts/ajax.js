"use strict";

function loadTemplate(url, target) {
    return fetch(url)
        .then(response => {
            if (response.status === 200) {
                return response.text()
            } else {
                throw new Error(`Bad response (status: ${response.status}`)
            }
        })
        .then(template => {
            let $target;

            if (target instanceof jQuery) {
                $target = target;
            } else if (_.isString(target)) {
                $target = $(target);
            } else {
                throw new Error("Target type is unsupported")
            }

            $target.html(template);
        })
        .catch((error) => {
            console.error("Can't load template data. Error: ", error);
            this.reject(error);
        });
}