// FileName: application.js
//= require jquery/jquery-3.1.1.slim
//= require metisMenu/metisMenu
//= require raphael/raphael
//= require morrisjs/morris
//= require sb-admin-2/javascripts/sb-admin-2
//= require bootstrap/javascripts/bootstrap
//= require datatables/js/jquery.dataTables
//= require datatables-plugins/dataTables.bootstrap
//= require datatables-responsive/dataTables.responsive
//= require underscore/underscore
//= require select2-4.0.3/js/select2.bundle
//= require inputmask/jquery.inputmask.bundle
//= require chartist/chartist.js
//= require ajax
//= require dashboard/cards
//= require_self
