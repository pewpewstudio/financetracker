BEGIN;

CREATE TABLE users (
  id            BIGSERIAL PRIMARY KEY NOT NULL,
  login         VARCHAR(30)           NOT NULL UNIQUE,
  password_hash CHAR(64)              NOT NULL,
  registered    TIMESTAMP             NOT NULL
);

CREATE TABLE accounts (
  id          BIGSERIAL PRIMARY KEY        NOT NULL,
  user_id     BIGINT REFERENCES users (id) NOT NULL,
  name        VARCHAR(40) UNIQUE           NOT NULL,
  card_number CHAR(16)                     NOT NULL,
  base_sum    DECIMAL(19, 2)               NOT NULL
);

CREATE TABLE operations (
  id          BIGSERIAL PRIMARY KEY           NOT NULL,
  account_id  BIGINT REFERENCES accounts (id) NOT NULL,
  created     TIMESTAMP                       NOT NULL,
  value       NUMERIC(19, 2)                  NOT NULL,
  description VARCHAR(400)                    NOT NULL
);

CREATE TABLE tags (
  id      BIGSERIAL PRIMARY KEY        NOT NULL,
  name    VARCHAR(20)                  NOT NULL,
  user_id BIGINT REFERENCES users (id) NULL
);

CREATE TABLE operations_tags (
  operation_id BIGINT REFERENCES operations (id) NOT NULL,
  tag_id       BIGINT REFERENCES tags (id)       NOT NULL,
  PRIMARY KEY (operation_id, tag_id)
);

COMMIT;